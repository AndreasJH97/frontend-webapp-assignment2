

/**
 * This function takes a key and a value, and saves the value to local storage using the key.
 * @param key - The key to store the data under.
 * @param value - The value to be saved.
 */
export const storageSave =(key, value)=>{
    localStorage.setItem(key, JSON.stringify(value))
}

/**
 * It reads a value from localStorage, and if it exists, it returns the value, otherwise it returns
 * null.
 * @returns The data is being returned as a string.
 */
export const storageRead = key => {
    const data = localStorage.getItem(key)
    if(data){
        return JSON.parse(data)
    }
    return null
}

/**
 * It deletes a key from localStorage.
 */
export const storageDelete = key =>{
    localStorage.removeItem(key)
}