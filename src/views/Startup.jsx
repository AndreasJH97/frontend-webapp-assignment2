import LoginForm from "../components/startup/LoginForm"
import '../styling/startup.css';

const Startup =() => {
    return(
        <div id="startup">
            
            <h1>Login</h1>
            <LoginForm />
        </div>
        
    )
}
export default Startup