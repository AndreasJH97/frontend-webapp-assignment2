import TranslationForm from '../components/Translation/TranslationForm';
import { useUser } from '../context/UserContext';
import withAuth from '../hoc/withAuth';
import '../styling/translation.css';

const Translation =() => {

  const {user} = useUser()

    return(
        <>
        <h1 id="oink">What do you want to translate, {user.username}?</h1>
        <TranslationForm user={user}/> 
        </>
    )
}
export default withAuth(Translation)