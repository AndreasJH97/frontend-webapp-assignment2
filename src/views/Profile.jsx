import ProfileActions from "../components/Profile/ProfileActions"
import ProfileHeader from "../components/Profile/ProfileHeaders"
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import '../styling/profile.css';

const Profile =() => {

    const {user} = useUser()

    return(
        <>
        <ProfileHeader username={user.username}/>
        <ProfileTranslationHistory translations={user.translations} />
        <ProfileActions />
        </>
    )
}
export default withAuth(Profile)