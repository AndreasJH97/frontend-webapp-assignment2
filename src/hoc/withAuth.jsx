import {Navigate} from "react-router-dom"
import { useUser } from "../context/UserContext"

/**
 * If the user is logged in, render the component, otherwise redirect to the home page.
 */
const withAuth = Component => props => {
    const {user} = useUser()
    if (user !== null) {
        return <Component {...props} />
    } else{
        return <Navigate to="/" />
    }
}

export default withAuth