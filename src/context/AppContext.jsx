/**
 * The AppContext function is a wrapper for the UserProvider function, which is a wrapper for the
 * children prop.
 * @returns The children of the AppContext component.
 */
import UserProvider from "./UserContext"

const AppContext =({children}) => {
    return(
        <UserProvider>
            {children}
        </UserProvider>
    )
}

export default AppContext