
import { useState } from 'react'
import { useContext } from 'react'
import {createContext} from 'react'
import { STORAGE_KEY_USER } from '../const/storageKeys'
import { storageRead } from '../utils/storage'

//Context ->exposing state
const UserContext = createContext()

export const useUser=()=>{
    return useContext(UserContext)//Returns object {user, setUser}
}

//Provider -> managing state
/**
 * The UserProvider function is a React component that provides the user state to all of its children.
 * @returns The UserProvider is returning the UserContext.Provider with the value of state.
 */
const UserProvider=({children})=> {
    const [user, setUser]=useState(storageRead(STORAGE_KEY_USER))
    const state ={
        user,
        setUser
    }

    return(
        <UserContext.Provider value={state}>
            {children}
        </UserContext.Provider>
    )
}
export default UserProvider


