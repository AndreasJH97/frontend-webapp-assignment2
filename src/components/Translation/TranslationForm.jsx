import { translationAdd } from "../../api/translation"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { storageRead, storageSave } from "../../utils/storage"
import { useUser } from '../../context/UserContext';

const { useState } = require("react")

const TranslationForm =()=>{

    const {user, setUser} = useUser()

    const [textToTranslate, setTextToTranslate]= useState("")
    const [preTranslate, setPreTranslate]= useState("")
    const [signList, setSignList]= useState([])

    /**
     * It takes a string, checks if it contains any numbers, special characters, or whitespaces, and if
     * it doesn't, it adds it to a list of strings.
     */
    const handleSubmit = async(event) => {
        let containsInt = false
        let tempSignList = []
        event.preventDefault();
        if(textToTranslate!==""){
            for (let index = 0; index < textToTranslate.length; index++) {
                if ( /\d|\W/.test(textToTranslate[index])) {
                    containsInt = true
                    alert("We cannot translate numbers, weird letters, special characters or whitespaces to sign language :(")
                    setTextToTranslate("")
                    break
                }else{
                    tempSignList.push(textToTranslate[index].toLowerCase())
                }
            }
            if(!containsInt){
                setSignList(tempSignList)
                setPreTranslate(textToTranslate)
                setTextToTranslate("")
                let oldObj = storageRead(STORAGE_KEY_USER)
                oldObj.translations.push(textToTranslate)
                setUser(oldObj)
                storageSave(STORAGE_KEY_USER, oldObj)
                const [error, result] = await translationAdd(user, textToTranslate)
                console.log("Error: "+ error)
                console.log("result: "+result)
            }
        }
    }  

    return(
        <>
        <form id="trans-form" onSubmit={handleSubmit}>
            <input className="field"type="text" value={textToTranslate} onChange={(e)=>setTextToTranslate(e.target.value)} />
            <input className="button-24" type="submit" />
        </form>
        {preTranslate && <p>"{preTranslate}" translates to: </p>}
        {signList.length > 0 && <div>{signList.map((value, index)=>{
            return <img src={require('../../individial_signs/'+value+'.png')} key={index} alt='Sign' height='40' width='40'></img>
        })}</div>} 

        </>
    )
}

export default TranslationForm