import {useState, useEffect} from 'react'
import { useNavigate } from 'react-router-dom';
import { storageSave } from '../../utils/storage';
import{loginUser} from '../../api/user'
import { useUser } from '../../context/UserContext';
import { STORAGE_KEY_USER } from '../../const/storageKeys';

const LoginForm= () => {
  //Local State
  const [username, setUserName]= useState("");
  const[loading, setLoading]= useState(false)
  const {user, setUser}= useUser()
  const navigate= useNavigate()

/* It's a hook that is called when the user changes. If the user is not null, it navigates to the
translation page. */
  useEffect(() =>{
    console.log('User has changed: '+ user)
    if (user !== null){
      navigate('translation')
    }

  }, [user, navigate])//Empty Deps, only run 1ce

  /**
   * It's a function that is called when a user clicks a button. It calls another function that makes a
   * request to an API. If the request is successful, it saves the response to the browser's local
   * storage.
   */
  const startLoginProcess=async(event)=>{
    setLoading(true)
    event.preventDefault();
    const [error, userResponse]=await loginUser(username)
    console.log('Error: ', error)
    console.log('User: ', userResponse)
    setLoading(false)
    //Navigating with name as parameter
    if(userResponse !==null){
      //Saving logged in user to browsers storage
      storageSave(STORAGE_KEY_USER, userResponse)
      setUser(userResponse)
    }else{
      alert("You must write a name")
    }
  }

    return(
      <>
        <div className="form">
          <form>
            <div className="input-container">
              <label><b>Userame: </b></label>
              <input className="loginfield" type="text" name="uname" value={username} onChange={(e)=>setUserName(e.target.value)} required />
            </div>
            <div className="button-container">
              <input className="loginbutton-24" type="submit" disabled={loading} onClick={startLoginProcess}/>
            </div>
            {loading && <p>Logging in ...</p>}
          </form>
        </div> 
        </>
        )

}

export default LoginForm;