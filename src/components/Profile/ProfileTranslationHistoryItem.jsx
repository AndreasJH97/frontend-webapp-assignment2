/**
 * This function takes a translation as a prop and returns a list item with the translation as the
 * content.
 * @returns A React component.
 */
const ProfileTranslationsHistoryItem = ({translation})=>{
    return <li>{translation}</li>
}
export default ProfileTranslationsHistoryItem