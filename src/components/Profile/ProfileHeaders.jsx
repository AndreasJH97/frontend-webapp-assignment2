const ProfileHeader =({username}) => {
    return(
        <div className="four">
            <h1><span>Profile</span> <br></br>
            Welcome back, <em>{username}</em></h1>
        </div>

    )
}
export default ProfileHeader