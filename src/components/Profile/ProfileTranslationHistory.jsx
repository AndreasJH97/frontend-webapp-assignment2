import ProfileTranslationsHistoryItem from "./ProfileTranslationHistoryItem"

/**
 * The ProfileTranslationHistory function takes in an array of translations and returns a section with
 * a title and a list of the last 10 translations in reverse order.
 */
const ProfileTranslationHistory =({translations}) => {

    const translationList= translations.slice(-10).reverse().map(
        (translation, index)=> <ProfileTranslationsHistoryItem key={index + "-" + translation} translation={translation}/>)
    return (
        <section>
            <h4>Your Translation History:</h4>
            <ul>
                {translationList}
            </ul>
        </section>
    )
}
export default ProfileTranslationHistory