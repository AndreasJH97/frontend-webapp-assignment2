import { useEffect } from "react"
import { profileUpdate } from "../../api/profile"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageDelete, storageSave } from "../../utils/storage"

const ProfileActions =() => {

    const {user, setUser} = useUser()

    useEffect(() =>{
        console.log('User has changed: '+ user)
        if (user !== null){
            storageSave(STORAGE_KEY_USER, user)
        }
    
      }, [user])//Empty Deps, only run 1ce



   const handleLogoutClick = () => {
        if(window.confirm("Are you sure you want to log out?")){
            //Send an event to the parent!'
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    const handleClearHistory =async ()=>{
        //update userContext
        setUser({username:user.username, translations:[], id:user.id})
        //Update localStorage -> Done though useEffect when user is updated
        //Update Api
        const [error, result] = await profileUpdate(user)
        console.log("Error: "+ error)
        console.log("result: "+result)
    }

    return(
        <div className="buttons">
            <button className="button-24" onClick={handleClearHistory}>Clear History</button>
            <button className="button-24" onClick={handleLogoutClick}>Logout</button>
        </div>
    )
}
export default ProfileActions