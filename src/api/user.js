
import {createHeaders} from './index'
const apiUrl= process.env.REACT_APP_API_URL


/**
 * It takes a username as an argument, makes a fetch request to the apiUrl, and returns an array with
 * the first element being an error message if there is one, and the second element being the data
 * returned from the api.
 * @param username - the username of the user you want to check for
 * @returns An array with two elements. The first element is an error message or null. The second
 * element is the data or an empty array.
 */
const checkForUser= async(username)=>{
    try{
        const response= await fetch(`${apiUrl}?username=${username}`)
        if (!response.ok){
            throw new Error("Could not complete request")
        }
        const data= await response.json()
        return[null, data]
    }catch(error){
        return [error.message, []]
    }
}

/**
 * It takes a username as an argument, and then it sends a POST request to the API with the username as
 * the body of the request. If the request is successful, it returns the data from the response. If the
 * request is unsuccessful, it returns an error message.
 * @param username - the username of the user to be created
 * @returns An array with two elements. The first element is an error message or null. The second
 * element is the data returned from the server.
 */
const createUSer= async(username)=>{
    try{
        const response= await fetch(apiUrl, {
            method:'POST',
            headers : createHeaders(),
            body: JSON.stringify({
                username,
                translations:[]})
        })
        if (!response.ok){
            throw new Error("Could not create user with username "+ username)
        }
        const data= await response.json()
        return[null, data]
    }catch(error){
        return [error.message, []]
    }
}

/**
 * It checks if a user exists, if it does, it returns the user, if it doesn't, it creates the user.
 * @param username - string
 * @returns An array with two elements. The first element is an error object or null. The second
 * element is a user object or null.
 */
export const loginUser= async(username)=>{
    const[checkError, user] = await checkForUser(username)
    if(checkError!==null){
        return[checkError, null]
    }
    if(user.length >0){
        return [null, user.pop()]
    }
    return await createUSer(username)
}