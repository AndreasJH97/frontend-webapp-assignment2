import { createHeaders } from "./"

const apiUrl= process.env.REACT_APP_API_URL

/**
 * It takes a user object, and updates the user's profile with the translations array.
 * @param user - {
 * @returns An array with two elements. The first element is an error message or null. The second
 * element is the result of the fetch or null.
 */
export const profileUpdate= async (user)=>{
    try{
        const response = await fetch(`${apiUrl}/${user.id}`,{
            method: 'PATCH',
                headers: createHeaders(),
                body: JSON.stringify({
                    translations:[]
                })
            })

        if(!response.ok){
            throw new Error("Could not update the user")
        }
        const result = await response.json()
        return[null, result]

    } catch(error){
        return[error.message, null]
    }
}