import { createHeaders } from "./"

const apiUrl= process.env.REACT_APP_API_URL

/**
 * It takes a user object and a translation object and adds the translation object to the user object's
 * translations array.
 * @param user - {
 * @param translation - "hello"
 * @returns [
 *   null,
 *   {
 *     "id": "5d8f8f8f8f8f8f8f8f8f8f8f",
 *     "translations": [
 *       "translation1",
 *       "translation2",
 *       "translation3",
 *       "translation4",
 *       "translation5",
 */
export const translationAdd= async (user, translation)=>{
        try{
            const response = await fetch(`${apiUrl}/${user.id}`,{
                method: 'PATCH',
                headers: createHeaders(),
                body: JSON.stringify({
                    translations:[...user.translations, translation]
                })
            })

            if(!response.ok){
                throw new Error("Could not update the order")
            }
            const result = await response.json()
            return[null, result]
    
        } catch(error){
            return[error.message, null]
        }
}