<!-- ABOUT THE PROJECT -->
## Sign Language Translator
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://gitlab.com/AndersRingen/chinook/-/blob/main/README.md)
[![pipeline-satus](https://gitlab.com/AndersRingen/chinook/badges/main/pipeline.svg)]()

In this project Anders and Andreas made a sign language translator as a single page application using React.js. We want to thank Livinus and Dewald for teaching us the knowledge and Heroku for hosting our servers. 


### Built With
This is made with React.js in the Visual Studio Code, a Heroku hosted API for storing data and Figma for drawing wireframes.
* [React](https://reactjs.org/)
* [Visual Studio Code](https://code.visualstudio.com/)
* [Live Share Extension Pack](https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare-pack)
* [Figma](https://www.figma.com/)


<!-- USAGE EXAMPLES -->
# Explaination

On this application, one is met with a login page. This is where the user can either make a new account or log in to an existing one by typing in its username. The user is then directed to the translation page where one can translate any word it wants. The word needs to only contain letters from a to z and no whitespace. 

To the right on the navbar on the top, the user is given a possibility to navigate between the translation page and the profile page. If the user chooses to click the "Profile" button on the navbar, it is directed to the profile page. Here one can see the users history of the last 10 translations. Further down on the page, the user is given the opportunity to click two different buttons. The "Clear history" button will clear the translation history of that user. The other button is the "logout" button, which will log the user out by removing its session cookie and directing it to the login page. 


# Approach

### Pt.1: Drawing Wireframes Using Figma

The first part of this project was the planning. This phase included getting every component of the app drawn out in a component tree in Figma before starting to make the application. This made it easier for us to plan what to make first and how we were going to make the different components. Here is the [Component Tree Diagram](https://gitlab.com/AndreasJH97/frontend-webapp-assignment2/-/blob/main/Component_Tree_Figma.pdf).

### Pt.2: Creating the Translation Application

In this second part, we followed directions given to us by lecturer Livinus, and making the application component for component. We made the components for the different pages and but them into the "views" folder and populated these components with the smaller components shown in the Tree Diagram. Examples of these smaller components are on the profile page that includes "clear history" and "logout" buttons as one, and the translation history as another component. This is how the entire application is build up.

The buttons calls its onSubmit-property calls a method that sends the input as a fetch request to the heroku hosted API. 

*Soon to be fullstack world champions.*

